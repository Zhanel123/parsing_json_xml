import 'package:xml/xml.dart';

class Contact {
  String _name, _email;
  int _age;

  Contact(this._name, this._email, this._age);

  factory Contact.fromJSON(Map<String, dynamic> json) {
    return Contact(json['name'], json['email'], json['age']);
  }

  get name => this._name;

  get email => this._email;

  get age => this._age;

  factory Contact.fromXml(XmlElement xml) {
    String? name = xml.findElements('name').first.text;
    String? email = xml.findElements('email').first.text;
    int? age = int.parse(xml.findElements('age').first.text);

    return Contact(name, email, age);
  }

  toXml(XmlBuilder builder) {
    // builder.processing('xml', 'version="1.0"');

    builder.element('contact', nest: () {
      builder.element('name', nest: () {
        builder.text(this._name);
      });

      builder.element('email', nest: () {
        builder.text(this._email);
      });

      builder.element('age', nest: () {
        builder.text(this._age);
      });
    });

    return builder.buildDocument();
  }
}
