import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'contact.dart';
import 'package:xml/xml.dart';

import 'item.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(primarySwatch: Colors.deepPurple),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  //Спарсили JSON
  Future<List<Contact>> getContactsFromJson(BuildContext context) async {
    String jsonString = await DefaultAssetBundle.of(context)
        .loadString('assets/data/contacts.json');
    List<dynamic> raw = jsonDecode(jsonString);
    return raw.map((f) => Contact.fromJSON(f)).toList();
  }

  //Спарсили XML
  Future<List<Contact>> getContactsFromXML(BuildContext context) async {
    String xmlString = await DefaultAssetBundle.of(context)
        .loadString('assets/data/contact.xml');

    //parse
    var raw = XmlDocument.parse(xmlString);
    var elements = raw.findAllElements('contact');

    // return elements.map((element){
    //   return Contact(
    //       element.findElements('name').first.text,
    //       element.findElements('email').first.text,
    //       int.parse(element.findElements('age').first.text,)) ;
    // }).toList();

    return elements.map((e) => Contact.fromXml(e)).toList();
  }

  saveXml(BuildContext context) async {
    await File('').writeAsString(
      await toXml(context),
      mode: FileMode.append,
      flush: true,
    );
  }

  Future<String> toXml(BuildContext context) async {
    List<Contact> contacts = await getContactsFromXML(context);

    XmlBuilder builder = XmlBuilder();
    builder.processing('xml', 'version="1.0"');

    for (int i = 0; i < contacts.length; i++) {
      contacts[i].toXml(builder);
    }

    return builder.buildDocument().toXmlString();
  }

  Future<List<Item>> getItemFromXML(BuildContext context) async {
    String xmlString = await DefaultAssetBundle.of(context)
        .loadString('assets/data/sample.xml');

    var raw = XmlDocument.parse(xmlString);
    // print(raw);
    var elements = raw.findAllElements('ITEM').toList();
    // print(elements);

    // for(int i=0; i<elements.length; i++ ) {
    //   print(Item.fromXml(elements[i]));
    // }

    // return elements.map((e) => Item(
    //    name: e.getAttribute('Name') ?? ''
    // )).toList();
    toXml(context);
    return elements.map((e) => Item.fromXml(e)).toList();
  }

  const MyHomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Data Parser'),
      ),
      body: Container(
        child: Center(
          child: FutureBuilder(
            // future: getContactsFromJson(context),
            // future: getItemFromXML(context),
            future: getItemFromXML(context),
            builder: (context, data) {
              if (data.hasData) {
                List<Item>? contacts = data.data;

                return ListView.builder(
                    itemCount: contacts?.length,
                    itemBuilder: (context, index) {
                      return ListTile(
                        title: Text(
                          contacts?[index].name ?? '',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 25),
                        ),
                        // subtitle: Text(contacts?[index].email),
                      );
                    });
              } else {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
            },
          ),
        ),
      ),
    );
  }
}
