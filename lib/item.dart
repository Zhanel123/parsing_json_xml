import 'package:xml/xml.dart';

class Item {
  String name;

  Item({
    this.name = '',
  });

  factory Item.fromXml(XmlElement xml) {
    String? name = xml.getAttribute('Name');
    return Item(name: name ?? '');
  }
}
